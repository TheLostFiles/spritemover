﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rb2d; // sets the variable to that component 
    public float speed; // makes speed changeable in the editor
    private bool moving; // makes a bool that is used to stop the movement when needed
    private static Quaternion rotation; // sets the Quaternion to rotation so that i can use the name
    private float rotationResetSpeed = 1f; // sets the rotation reset speed to 1

    void Start() // runs at the start of the program running
    {
        rb2d = GetComponent<Rigidbody2D>(); // sets the name of the component so that I can use it easily 
        rotation = transform.rotation; // makes it so i can use the transform rotation easier
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving) // makes it so i can change the movement of the Game Object
        {
            if (Input.GetKey("left shift")) // checks if the left shift key is pressed
            {
                if (Input.GetKeyDown("d")) //checks if the d key is pressed 
                {
                    transform.Translate(1, 0, 0); // makes the game object only move one unit
                }

                if (Input.GetKeyDown("w"))//checks if the w key is pressed 
                {
                    transform.Translate(0, 1, 0); // makes the game object only move one unit
                }

                if (Input.GetKeyDown("a"))//checks if the a key is pressed 
                {
                    transform.Translate(-1, 0, 0); // makes the game object only move one unit
                }

                if (Input.GetKeyDown("s"))//checks if the s key is pressed 
                {
                    transform.Translate(0, -1, 0); // makes the game object only move one unit
                }
            }
            else
            {
                float moveHorizontal = Input.GetAxis("Horizontal"); // gets the user input for the horizontal axes
                float moveVertical = Input.GetAxis("Vertical"); // gets the user input for the vertical axes
                Vector2 movement = new Vector2(moveHorizontal, moveVertical); // sets movement to a vector2 per user input
                rb2d.AddForce(movement * speed); // adds force to the correct direction
            }
        }

        if (Input.GetKey("space"))//checks if the space key is pressed 
        {
            rb2d.angularDrag = 10000000000000000; // it works with this number and this is where it's staying
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.time * rotationResetSpeed); // resets the rotation
            transform.position = Vector3.zero; // sets the position back to zero
            
        }
        if (Input.GetKeyUp("space"))//checks if the space key is un-pressed 
        {
            rb2d.angularDrag = 0; // sets the angular drag back to zero from 10000000000000000
        }

        if (Input.GetKeyDown("p"))//checks if the p key is pressed 
        {
            moving = !moving; //toggles movement bool
        }

        if (Input.GetKeyDown("q"))//checks if the q key is pressed 
        {
            gameObject.SetActive(false); // sets the game object to inactive
        }

        if (Input.GetKeyDown("escape"))//checks if the escape key is pressed 
        {
            Application.Quit(); // exits application0
        }
    }
}
    
    
             
